const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const customerSch = new Schema({
    customerId: {
        type: String
    },
    name: {
        type: String,
        required: true
    },
    phoneNumber: {
        type: Number,
        required: true
    },
    type: {
        type: String,
        default: 'customer'
    },
    status: {
        type: String,
        enum: ['Active', 'Inactive'],
        default: 'Active'
    },
});

customerSch.index({
    'name': 'text'
});

module.exports = mongoose.model('customers', customerSch);