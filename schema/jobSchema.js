const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const jobSch = new Schema({
    token : {type:Number},
    name : {type:String},
    phoneNumber: {type:Number},
    gender : {type:String},
    stage : {type:String},
    categoryId : [Schema.Types.ObjectId],
    subCategoryId : [Schema.Types.ObjectId],
    serviceId : [Schema.Types.ObjectId],
    category : [String],
    subcategory : [String],
    service: [String],
    paymenttype: [String],
    tax: {type:Number},
    totalprice: {type:String},
    servicePerson: {type:String},
    createdOn : {type:Date, default:new Date()},
    updatedOn : {type:Date, default:new Date()}   
    
});
module.exports = mongoose.model('jobs', jobSch);