const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const moment = require('moment');

const empPreSch = new Schema({
    empId: Schema.Types.ObjectId, 
    employeeID : {type:String},//manually created
    employee:{},
    companyName: {type: String},
    phoneNumbers: {type: String},
    companyaddress: {type: String},
    area: {type: String},
    salary: {type: String},
    createdOn:{type:Date, default:new Date()},
    updatedOn:{type:Date, default:Date.now},
});

module.exports = mongoose.model('empPrevious', empPreSch);

