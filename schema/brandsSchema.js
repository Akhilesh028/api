const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const brandSch = new Schema({
    createdOn: {
        type: Date,
        default: new Date()
    },
    updatedOn: {
        type: Date,
        default: Date.now
    },
    apiKey: {
        type: String
    },
    serialNo: {
        type: Number
    },
    orgKey: {
        type: String
    },
    orgId: {
        type: String
    },
    brandId: {
        type: String
    },
    brandName: {
        type: String
    },
    ownerName: {
        type: String
    },
    phoneNumber: {
        type: String
    },
    emailId: {
        type: String
    },
    address: {
        type: String
    },
    landmark: {
        type: String
    },
    totalShops: {
        type: Number
    },
    role: {
        type: String
    },
    status: {
        type: String,
        enum: ["Active", "Inactive", "Rejected"],
        default: "Active"
    }
})

module.exports = mongoose.model("brands", brandSch);