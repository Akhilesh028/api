const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const empSch = new Schema({
    employeeID: {
        type: String
    },
    name: {
        type: String
    },
    phoneNumber: {
        type: String
    },
    email: {
        type: String
    },
    gender: {
        type: String
    },
    age: {
        type: String
    },
    status: {
        type: String,
        enum: ['Active', 'Inactive'],
        default: 'Active'
    },
    specialities: {
        categories: [],
        services: []
    },
    createdOn: {
        type: Date,
        default: new Date()
    },
    updatedOn: {
        type: Date,
        default: Date.now
    },
});

empSch.index({
    name: 'text'
});


module.exports = mongoose.model('employees', empSch);