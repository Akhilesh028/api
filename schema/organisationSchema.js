const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const orgSchema = new Schema({
    createdOn: {
        type: Date,
        default: new Date()
    },
    updatedOn: {
        type: Date,
        default: Date.now
    },
    apiKey: {
        type: String
    },
    orgKey: {                   //customised generated key
        type: String
    },
    orgName: {
        type: String
    },
    officeAddress: {}
});

module.exports = mongoose.model("org", orgSchema);