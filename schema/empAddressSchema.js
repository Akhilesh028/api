const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const empAddSch = new Schema({
    empId: Schema.Types.ObjectId,
    employeeID: {type:String},
    employee:{},
    addressProofs:[],
    houseAddress: {type: String},
    lane: {type: String},
    landmark: {type: String},
    pinCode: {type: String},
    createdOn:{type:Date, default:new Date()},
    updatedOn:{type:Date, default:Date.now},
});

module.exports = mongoose.model('empAddress', empAddSch);
