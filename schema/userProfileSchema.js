const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const userProfileSch = new Schema({
    createdOn: {
        type: Date,
        default: new Date()
    },
    updatedOn: {
        type: Date,
        default: Date.now
    },
    orgApiKey: {
        type: String
    },
    brandId: {
        type: String
    },
    brandApiKey: {
        type: String
    },
    brandName: {
        type: String
    },
    emailId: {
        type: String
    },
    name: {
        type: String
    },
    username: {
        type: String
    },
    password: {
        type: String
    },
    permissions: {},
    role: {
        type: String
    },
    roles: {
        type: String,
        enum: ["employee", "manager"]
    }
})

module.exports = mongoose.model('userProfile', userProfileSch);