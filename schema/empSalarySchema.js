const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const empSalSch = new Schema({
    employeeID: Schema.Types.ObjectId,
    employee:{},
    salaryDecided : {type: String},
    bonus: {type: String},
    targets: {type: String},
    workingUnder: {type: String},
    createdOn : {type:Date, default:new Date()},
    updatedOn : {type:Date, default:new Date()},
});

module.exports = mongoose.model('empSalary', empSalSch);