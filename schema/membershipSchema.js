const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const membershipSch = new Schema({
    membershipId: {
        type: Number
    },
    // custId: Schema.Types.ObjectId,
    // customerId: {
    //     type: String
    // },
    // customer: {},
    name: {
        type: String
    },
    type: {
        type: String
    },
    typeValue: {
        type: Number
    },
    // mduration: {
    //     type: String
    // },
    // pduration: {
    //     type: String
    // },
    description: {
        type: String
    },
    // customerPlan: {
    //     category: [],
    //     service: [],
    //     price: {
    //         type: Number
    //     }
    // },
    createdOn: {
        type: Date,
        default: new Date()
    },
    updatedOn: {
        type: Date,
        default: new Date()
    }
})

module.exports = mongoose.model('membership', membershipSch);