const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const serviceSch = new Schema({
    serialNo: {
        type: Number
    },
    categoryId: Schema.Types.ObjectId,
    category: {},
    name: {
        type: String
    },
    rate: {
        type: Number
    },
    type: {
        type: String
    },
    gst: {
        type: Number
    },
    discount: {
        type: Number
    },
    discountedRate: {
        type: Number
    },
    membershipId: Schema.Types.ObjectId,
    membership: {},
    membershipPrice: {
        type: Number
    },
    createdOn: {
        type: Date,
        default: new Date()
    },
    updatedOn: {
        type: Date,
        default: new Date()
    }
});
serviceSch.index({
    name: 'text'
});


module.exports = mongoose.model('services', serviceSch);