const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const moment = require('moment');

const empAttSch = new Schema({
    employeeID: Schema.Types.ObjectId,
    employee:{},
    shiftStartTime: {type: String},
    shiftEndTime: {type: String},
    overTimehrs: {type: String},
    monthlyAttendance: {type: String},
    createdOn : {type:Date, default:new Date()},
    updatedOn : {type:Date, default:new Date()}, 
});

module.exports = mongoose.model('empAttendance', empAttSch);