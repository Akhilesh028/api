const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const invoiceSch = new Schema({
    invoiceNumber: {
        type: String
    },
    customerId: {
        type: String
    },
    name: {
        type: String
    },
    phoneNumber: {
        type: Number
    },
    gender: {
        type: String
    },
    stage: {
        type: String
    },
    employeeAddressed: {
        employeeName: [],
        employeeEarned: []
    },
    jobsDone: [],
    membershipName: {
        type: String
    },
    serviceTotal: {
        type: Number
    },
    sgstApplied: {
        type: String
    },
    cgstApplied: {
        type: String
    },
    productTotal: {
        type: Number
    },
    totalBill: {
        type: Number
    },
    instantDiscount: {
        type: Number
    },
    amtPaid: {
        type: Number
    },
    pendingAmt: {
        type: Number
    },
    balanceAmt: {
        type: Number
    },
    description: {
        type: String
    },
    paymentStatus: {
        type: String
    },
    modePayment: [],
    paidIn: [],
    balAmtPaid: {
        type: Number
    },
    updatedAmt: {
        type: String
    },
    status: {
        type: String,
        enum: ['Active', 'Inactive'],
        default: 'Active'
    },
    createdOn: {
        type: Date,
        default: new Date()
    },
    updatedOn: {
        type: Date,
        default: new Date()
    }
});

invoiceSch.index({
    name: "text"
});



module.exports = mongoose.model('invoice', invoiceSch);