const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const moment = require('moment');
const membCustSch = new Schema({
    custId: Schema.Types.ObjectId,
    customerId: {
        type: String
    },
    customer: {},
    memberId: Schema.Types.ObjectId,
    memberhsipId: {
        type: String
    },
    startDate: {
        type: Date,
        default: new Date()
    },
    endDate: {
        type: Date
    },
    days: {
        type: Number
    },
    membership: {},
    createdOn: {
        type: Date,
        default: new Date()
    },
    updatedOn: {
        type: Date,
        default: new Date()
    }
}, {
    toObject: {
        virtuals: true
    },
    toJSON: {
        virtuals: true
    }
});

membCustSch.virtual("remainingDays").get(function () {
    let td = moment(new Date(), 'DD-MM-YYYY');
    let end = moment(new Date(this.endDate), 'DD-MM-YYYY');

    let diff = end.diff(td, 'days');
   // console.log('diff', diff)
    if (this.days > diff) {
        return diff;
    } else {
        return 'expired';
    }
})

// membCustSch.virtual("remainingDays").get(function(){
//     let td = moment(new Date(), 'DD-MM-YYYY');
//     let end = moment(new Date(this.endDate), 'DD-MM-YYYY');

//     let diff = end.diff(td,'days');
//     console.log('diff', diff)
//     if(this.days > diff) {
//         return diff;
//     }else {
//         return 'expired';
//     }
// })

//membCustSch.findByIdAndUpdate(id, { createdOn : new Date() }, options, callback)

module.exports = mongoose.model('membCust', membCustSch);