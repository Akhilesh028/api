const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const taxSch = new Schema({
    taxid: {type:String},
    name: {type: String},
    value: {type: Number},
    type: {type: String},
    createdOn: {type: Date, default: new Date()},
    updatedOn: {type: Date, default: new Date()}
});

module.exports = mongoose.model('taxes', taxSch);