const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const leadSch = new Schema({
    user: {},
    token: {
        type: Number
    },
    userId: Schema.Types.ObjectId,
    gender: {
        type: String
    },
    stage: {
        type: String
    },
    service: {
        type: String
    },
    status: {
        type: String,
        enum: ['Open', 'Inprogress', 'Completed', 'Rejected'],
        default: 'Open'
    },
    createdOn: {
        type: Date,
        default: new Date()
    },
    updatedOn: {
        type: Date,
        default: Date.now
    },
    //servicePerson:{type:String}
});

module.exports = mongoose.model('leads', leadSch);