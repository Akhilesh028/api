const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const shopSchema = new Schema({
    createdOn: {
        type: Date,
        default: new Date()
    },
    updatedOn: {
        type: Date,
        default: Date.now
    },
    shopKey: { //customised key of the brand
        type: String
    },
    brandId: {
        type: String
    },
    brandDetails: {},
    shopName: {
        type: String
    },
    phoneNumber: {
        type: Number
    },
    emailId: {
        type: String
    },
    address: {},
    landmark: {
        type: String
    },
    pincode: {
        type: Number
    },
    status: {
        type: String,
        enum: ['Active', 'Inactive', 'Deleted'],
        default: 'Active'
    }
})

module.exports = mongoose.model("shops", shopSchema);