const jwt = require('jsonwebtoken');
// const UserProfile = require('../schema/userProfileSchema');


module.exports.verifyToken = (req, res, next) => {
    const token = req.headers['authorization'];
    //console.log("token", token);
    if (!token) return res.status(401).send('Access denied');

    try {
        const verified = jwt.verify(token, 'autopylo@key');
        //console.log("verified token === ", verified);
        //console.log("req.user = verified", req.user = verified);
        req.user = verified;
        next();
    } catch (err) {
        res.status(400).send("Token expired");
    }

    // if (typeof token !== 'undefined') {
    //     jwt.verify(token, 'autopylo@key', (err, authData) => {
    //         if (err) {
    //             res.json({
    //                 "error": err.name,
    //                 "message": err.message
    //             })
    //         } else {
    //             console.log(authData)
    // UserProfile.find({
    //     username: authData.username
    // }).then((user) => {
    //     if (user !== null) {
    //         reject(err)
    //     } else {
    //         resolve
    //     }
    // }).catch(err => reject(err))
    // res.json({
    //     "message": 'Logged in successfully',
    //     authData
    // })
    // }
    // });


    // } else {
    // res.sendStatus(403);
    // }
    // })
}