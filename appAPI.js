// Import Modules
const express = require('express');
const app = express();
const config = require('./config/config');
const bodyParser = require('body-parser');
const customer = require('./controllers/customerController');
const category = require('./controllers/categoryController');
const service = require('./controllers/serviceController');
const lead = require('./controllers/leadController');
const job = require('./controllers/jobController');
const employee = require('./controllers/employeeController.js');
const empAddress = require('./controllers/empAddressController');
const empAttendance = require('./controllers/empAttendanceController');
const empPrejobDetails = require('./controllers/empPreviousJobDetailsController');
const empSalary = require('./controllers/empSalaryController');
const membership = require('./controllers/membershipController');
const membershipCustomer = require('./controllers/membCustController');
const taxes = require('./controllers/taxController');
const invoices = require('./controllers/finalInvoiceController');
const organisation = require('./controllers/organisationController');
const brands = require('./controllers/brandsController');
const shops = require('./controllers/shopsController');
const userProfile = require('./controllers/userProfileController');
const login = require('./controllers/loginController');
const middleware = require('./middleware/verifyToken');
const mongoose = require('mongoose');
const morgan = require('morgan');
const middles = require('./middleware/errorhandler');
const cors = require('cors');

mongoose.Promise = global.Promise
console.log("config.db.url", config.db.url);
mongoose.connect(config.db.url, {
        useNewUrlParser: true
    })
    .then(
        data => console.log('Connected with db')
    )
    .catch(
        err => console.log('Issue connecting with db =>', err)
    )
mongoose.set('useCreateIndex', true);

// Register static resource
app.use(morgan('tiny', {}))
app.use(cors());
app.use(middles.logger);
//app.use(middleware.verifyToken);
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: false
}))

// App Routes
app.get('/', (req, res) => {
    res.send("API is up & running");
})
app.use('/customer', middleware.verifyToken, customer)
app.use('/categories', middleware.verifyToken, category);
app.use('/services', middleware.verifyToken, service);
app.use('/leads', middleware.verifyToken, lead);
app.use('/jobs', middleware.verifyToken, job);
app.use('/employees', middleware.verifyToken, employee);
app.use('/empAddress', middleware.verifyToken, empAddress);
app.use('/empAttendance', middleware.verifyToken, empAttendance);
app.use('/empPrejobDetails', middleware.verifyToken, empPrejobDetails);
app.use('/empSalary', middleware.verifyToken, empSalary);
app.use('/membership', middleware.verifyToken, membership);
app.use('/membershipcustomer', middleware.verifyToken, membershipCustomer);
app.use('/taxes', middleware.verifyToken, taxes);
app.use('/invoices', middleware.verifyToken, invoices);
app.use('/organisation', organisation);
app.use('/brands', middleware.verifyToken, brands);
app.use('/shops', middleware.verifyToken, shops);
app.use('/userProfile', middleware.verifyToken, userProfile);
app.use('/login', login);


app.use(middles.notFound);
app.use(middles.errors);

app.listen(3011, () => console.log('Server is running on port 3011'));