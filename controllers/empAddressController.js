const router = require('express').Router();
const EmpAddress = require('../schema/empAddressSchema');
const Employee = require('../schema/employeeSchema');


router.post('/', async (req, res, next) => {
    let empadd = req.body;
    let filter = {};
    if (!empadd.employeeID) {
        res.status(400).json({
            message: "EmployeeId not found"
        })
    }
    if (req.body.employeeID) {
        filter.employeeID = req.body.employeeID
    };
    try {
        Employee.find(filter)
            .then(employee => {
               // console.log("find employee", employee)
                if (employee && employee != null) {
                    empadd.employee = employee
                    let newEmployee = new EmpAddress(empadd);
                    newEmployee.empId = employee[0]._id;
                    newEmployee.save()
                        .then(empadd => {
                            res.status(201).json({
                                message: 'Employee Address created successfully',
                                empadd
                            })
                        }).catch(err => next(err))
                } else {
                    res.status(404).json({
                        message: 'Employee not found'
                    })
                }
            }).catch(err => next(err))
    } catch (error) {
        next(error)
    }
})

router.get('/:id', (req, res, next) => {
    var id = req.params.id;
    EmpAddress.findById(id)
        .then(id => {
           // console.log(id)
            res.status(200).json({
                data: id
            })
        })
});

router.get('/', async (req, res, next) => {
    var fil = {};
  //  console.log(req.query);

    if (req.query.empname) {
        fil.empname = req.query.empname
    };
    if (req.query.pinCode) {
        fil.pinCode = req.query.pinCode
    };
    try {
     //   console.log('hiiii')
        let emp = await EmpAddress.find(fil).sort(req.query.sort);
        res.status(200).json({
            data: emp
        })
    } catch (error) {
        next(error)
    }
});

router.delete('/:id', (req, res, next) => {
    let id = req.params.id;
    EmpAddress.findByIdAndDelete(id).then((emp) => {
        res.status(200).json({
            message: 'Emp Address deleted',
            data: emp
        });
    }).catch(err => next(err))
});

router.put('/:id', (req, res, next) => {
    let id = req.params.id;
    let empObj = req.body;
    EmpAddress.findByIdAndUpdate(id, {
                $set: empObj,
                $inc: {
                    __v: 1
                }
            },
            {
                new: true
            })
        .then((data) => {
            res.json({
                data: data
            })
        }).catch(err => next(err))
})


module.exports = router;