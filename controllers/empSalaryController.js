const router = require('express').Router();
const EmpSalary = require('../schema/empSalarySchema');
const Employee = require('../schema/employeeSchema');

router.post('/', async (req, res, next) => {
    let empsalary = req.body;
    if (!empsalary.employeeId) {
        res.status(400).json({
            message: "EmployeeId not found"
        })
    }
    Employee.findById(empsalary.employeeId)
        .then(employee => {
            if (employee && employee != null) {
                empsalary.employee = employee
                let newEmployee = new EmpSalary(empsalary);
                newEmployee.save()
                    .then(empsalary => {
                        res.status(201).json({
                            message: 'Employee Salary details created successfully',
                            empsalary
                        })
                    }).catch(err => next(err))
            } else {
                res.status(404).json({
                    message: 'Employee not found'
                })
            }
        }).catch(err => next(err))
})


router.get('/', async (req, res, next) => {
    var fil = {};
    // console.log(req.query);
    if (req.query.empName) {
        fil.empName = req.query.empName
    };
    if (req.query.salaryDecided) {
        fil.salaryDecided = req.query.salaryDecided
    };
    try {
        // console.log('hiiii')
        let empsal = await EmpSalary.find(fil).sort(req.query.sort);
        res.status(200).json({
            data: empsal
        })
    } catch (error) {
        next(error)
    }
});

router.delete('/:id', (req, res, next) => {
    let id = req.params.id;
    EmpSalary.findByIdAndDelete(id).then((empsal) => {
        res.status(200).json({
            message: 'Emp Salary Details deleted',
            data: empsal
        });
    }).catch(err => next(err))
});


router.put('/:id', (req, res, next) => {
    let id = req.params.id;
    let empObj = req.body;
    EmpSalary.findByIdAndUpdate(id, {
            $set: empObj,
            $inc: {
                __v: 1
            }
        }, {
            new: true
        })
        .then((data) => {
            res.json({
                message: "Emp Salary Details Update Successfully",
                data: data
            })
        }).catch(err => next(err))
})

module.exports = router;