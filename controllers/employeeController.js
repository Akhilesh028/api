const router = require('express').Router();
const Employee = require('../schema/employeeSchema');


router.get('/', async (req, res, next) => {
    var filter = {};
    console.log(req.query);
    if (req.query.phoneNumber) {
        filter.phoneNumber = req.query.phoneNumber
    };
    if (req.query.name) {
        filter.name = req.query.name
    };
    if (req.query.status) {
        filter.status = req.query.status
    };
    try {
        let employee;
        let findEmp;
        let foundDocu;
        let empID;
        let count;
        let emplists;
        let pagesz;
        let pagegno;
        let query = req.query.name;
        // console.log("filter", filter);
        // console.log("query", query);
        count = await Employee.find(filter).countDocuments();
        if (query) {
            findEmp = await Employee.find({
                $text: {
                    $search: query
                }
            });
            // console.log("findEmp", findEmp);
            foundDocu = await Employee.find({
                $text: {
                    $search: query
                }
            }).countDocuments();
            // console.log("foundDocu", foundDocu);
        } else {
            employee = await Employee.find(filter).sort(req.query.sort);
            count = await Employee.find(filter).countDocuments();
            empID = await Employee.findOne(filter).sort({
                _id: -1
            });
            // console.log("empID", empID);

            pagesz = parseInt(req.query.pagesz) || 10;
            pagegno = parseInt(req.query.pagegno) || 1;
            emplists = await Employee.find(filter).sort({
                employeeID: -1
            }).skip((pagesz * pagegno) - pagesz).limit(pagesz);
            // console.log("filter kya hai", filter);
            emplists.pagesz = pagesz;
            emplists.pagegno = pagegno;
            // console.log("lists Employee pagination", emplists);
        }
        res.status(200).json({
            data: employee,
            previousEmployee: empID,
            findEmp: findEmp,
            foundDocu: foundDocu,
            total: count,
            pagegno: pagegno,
            pagesz: pagesz,
            emplists: emplists
        })
    } catch (error) {
        next(error)
    }
});











router.post('/', async (req, res) => {
    var data = req.body;
    var employee = new Employee(req.body);
    employee.save().then(emp => {
        res.status(201).json({
            message: 'Employee saved succesfully',
            emp
        })
    }).catch(err => console.log(err));
});

// router.get('/', async (req, res, next) => {
//     console.log('Hello');
//     var fil = {};
//     console.log(req.query);
//     if (req.query.phoneNumber) {
//         fil.phoneNumber = req.query.phoneNumber
//     };
//     if (req.query.name) {
//         fil.name = req.query.name
//     };
//     try {
//         let employee = await Employee.find(fil).sort(req.query.sort);
//         let count = await Employee.find(fil).countDocuments();
//         let empID = await Employee.findOne(fil).sort({_id: -1});
//         console.log("empID", empID);
//         console.log("hiii wer is it printing again nd agian")
//         res.status(200).json({
//             data: employee,
//             previousEmployee: empID,
//             total: count
//         })
//     } catch (error) {
//         next(error)
//     }
// });


router.get('/:id', (req, res, next) => {
    var id = req.params.id;

    Employee.findById(id)
        .then(id => {
            console.log(id)
            res.status(200).json({
                data: id
            })
        })
})

router.put('/:id', (req, res, next) => {
    let id = req.params.id;
    let empObj = req.body;
    console.log(empObj);

    Employee.findByIdAndUpdate(id, {
            $set: empObj,
            $inc: {
                __v: 1
            }
        }, {
            new: true
        })
        .then((data) => {
            res.json({
                message: "Employee updated Successfully",
                data: data
            })
        }).catch(err => next(err))
})


router.delete('/:id', (req, res, next) => {
    let id = req.params.id;
    Employee.findByIdAndRemove(id).then((emp) => {
        res.status(200).json({
            message: 'Employee deleted',
            data: emp
        });
    }).catch(err => next(err))
});

module.exports = router