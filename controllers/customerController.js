const router = require('express').Router();
const Customer = require('../schema/customerSchema');

router.get('/', async (req, res, next) => {
    let filter = {};
    let fil = {};
    if (req.query.status) {
        filter.status = req.query.status
    };
    //console.log(req.query);
    try {
        let customers;
        let findQueries;
        let foundDoc;
        let lists;
        let pgsize;
        let pgno;
        let query = req.query.name;
        // console.log("filter", filter);
        // console.log("query", query);
        let count = await Customer.find(filter).countDocuments();
        if (req.query.phoneNumber) {
            fil.phoneNumber = req.query.phoneNumber
        };
        if (query) {
            findQueries = await Customer.find({
                $text: {
                    $search: query
                }
            });
            //  console.log("findQueries1", findQueries);
            foundDoc = await Customer.find({
                $text: {
                    $search: query
                }
            }).countDocuments();
            //  console.log("foundDoc", foundDoc);
        } else {
            //customers = await Customer.find(fil).sort({});
            pgsize = parseInt(req.query.pgsize) || 10;
            pgno = parseInt(req.query.pgno) || 1;
            lists = await Customer.find(filter).sort({
                customerId: -1
            }).skip((pgsize * pgno) - pgsize).limit(pgsize);
            // console.log("filter kya hai", filter);
            lists.pgsize = pgsize;
            lists.pgno = pgno;
            // console.log("lists customer pagination", lists);
        }
        //  console.log("findQueries", findQueries);
        res.status(200).json({
            //customers: customers,
            foundDoc: foundDoc,
            findQueries: findQueries,
            total: count,
            pgno: pgno,
            pgsize: pgsize,
            lists: lists
        })
    } catch (error) {
        next(error)
    }
});

router.get('/:id', (req, res, next) => {
    var id = req.params.id;
    //  console.log("id", id);
    Customer.findById(id)
        .then(id => {
            //  console.log(id)
            res.status(200).json({
                data: id
            })
        })
});

router.put('/:id', (req, res, next) => {
    let id = req.params.id;
    let custObj = req.body;

    Customer.findByIdAndUpdate(id, {
                $set: custObj,
                $inc: {
                    __v: 1
                }
            },

            {
                new: true
            })
        .then((data) => {
            res.json({
                data: data
            })
        }).catch(err => next(err))
});

router.post('/', (req, res) => {
    var customer = new Customer(req.body);
    customer.save().then(user => {
        res.status(201).json({
            message: 'Customer saved succesfully',
            user
        })
    }).catch(err => console.log(err));
});

router.delete('/:id', (req, res, next) => {
    let id = req.params.id;
    Customer.findByIdAndDelete(id).then((cust) => {
        res.status(200).json({
            message: 'Customer deleted',
            data: cust
        });
    }).catch(err => next(err))
});

module.exports = router;