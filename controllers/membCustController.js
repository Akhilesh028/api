const router = require('express').Router();
const MembershipCustomer = require('../schema/membCustSchema');
const Customer = require('../schema/customerSchema');
const Membership = require('../schema/membershipSchema');
const moment = require('moment');

router.post('/', async (req, res, next) => {
    let body = req.body;
   // console.log("body", body);
    let filter = {};
    let fil = {};
    if (!body.customerId) {
        res.status(400).json({
            message: "customerId not found"
        })
    }
    if (!body.membershipId) {
        res.status(400).json({
            message: "membershipId not found"
        })
    }
    if (req.body.customerId) {
        filter.customerId = req.body.customerId
     //   console.log("filter.customerId",filter.customerId)
    };
    if (req.body.membershipId) {
        fil.membershipId = req.body.membershipId
       // console.log("fil.membershipId", fil.membershipId)
    };
    try {
        let today = new Date();
        //console.log("today", today);
        let endDates = new Date(today.setDate(today.getDate() + parseInt(body.days)));
        //console.log("endDates", endDates);
        endDate = endDates;
        //console.log("endDates.endDate", endDate);
        let membership = await Membership.find(fil);
       // console.log("fil", fil)
       // console.log("membership", membership)
        body.membership = membership;
       // console.log("body.membership", body.membership);
        let custmerid = await Customer.find(filter);
        body.customer = custmerid;
        //console.log("body.customer", body.customer[0]);
        let newData = new MembershipCustomer(body);
        newData.endDate = endDates;
        //console.log('newData1', newData);
        newData.save().then(body => {
            res.status(201).json({
                message: 'Membership created successfully',
                body
            })
        }).catch(err => next(err))
    } catch (error) {
        next(error)
    }
})

router.get('/', async (req, res, next) => {
    var fil = {};
    //console.log("req.query", req.query);
    if (req.query.customerId) {
        fil.customerId = req.query.customerId
    };
    // if (req.query.customerId) {
    //     fil["user.customerId"] = req.query.customerId;
    // }
    try {
        let membership = await MembershipCustomer.find(fil).sort(req.query.sort);
      //  console.log("membership", membership); 


        res.status(200).json({
            message: "Membership & Customer plan data",
            data: membership
        })
    } catch (error) {
        next(error)
    }
});

router.get('/:id', (req, res, next) => {
    var id = req.params.id;

    MembershipCustomer.findById(id)
        .then(id => {
        //    console.log(id)
            res.status(200).json({
                message: "Membership & Customer plan data",
                data: id
            })
        })
});

router.delete('/:id', (req, res, next) => {
    let id = req.params.id;
    MembershipCustomer.findByIdAndDelete(id).then((membership) => {
        res.status(200).json({
            message: 'Membership & Customer plan deleted',
            data: membership
        });
    }).catch(err => next(err))
});

router.put('/:id', (req, res, next) => {
    let id = req.params.id;
    //console.log("id", id);
    let memberObj = req.body;
    //console.log("membership", memberObj);
    let body = req.body;
    //console.log("body", body.days);
    let today = new Date();
    let endDates = new Date(today.setDate(today.getDate() + body.days));
    //console.log("endDates", endDates);
    memberObj.endDate = endDates;
    //console.log(" memberObj.endDate", memberObj.endDate);
    MembershipCustomer.findByIdAndUpdate(id, {
            $set: memberObj,
            $inc: {
                __v: 1
            }
        }, {
            new: true
        })
        .then((data) => {
            res.json({
                message: "Membership & Customer plan successfully updated",
                data: data
            })
        }).catch(err => next(err))
});

module.exports = router;