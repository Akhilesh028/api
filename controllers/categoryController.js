const router = require('express').Router();
const Category = require('../schema/categorySchema');
const Customer = require('../controllers/customerController');

router.get('/', async (req, res) => {
    //var type = req.query.type
    var filter = {};
    //console.log(req.query);

    if (req.query.name) {
        filter.name = req.query.name
    };
    if (req.query.type) {
        filter.type = req.query.type
    }
    try {
        let count = await Category.find(filter).countDocuments();
        let categories = await Category.find(filter).sort(req.query.sort);
        res.status(200).json({
            data: categories,
            total: count
        })
    } catch (error) {
        next(error)
    }
});

router.put('/:id', async (req, res, next) => {
    let id = req.params.id;
    let categoryObj = req.body;

    Category.findByIdAndUpdate(id, {
        $set: categoryObj,
        $inc: {
            __v: 1
        }
    }, {
        new: true
    }).then((data) => {
        res.json({
            data: data
        })
    }).catch(err => next(err))
});

router.post('/', (req, res) => {
    let category = new Category(req.body);
    //console.log(category);
    category.save().then(category => {
        res.status(201).json({
            mesaage: 'category saved successfully',
            category
        })
    }).catch(err => console.log(err));
});

router.delete('/:id', (req, res, next) => {
    let id = req.params.id;
    Category.findByIdAndRemove(id).then((category) => {
        res.status(200).json({
            message: 'Category deleted',
            data: category
        });
    }).catch(err => next(err))
});


module.exports = router;