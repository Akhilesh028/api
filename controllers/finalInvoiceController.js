const router = require('express').Router();
const Invoice = require('../schema/finalInvoiceSchema');


router.post('/', (req, res) => {
    let body = req.body;
    // console.log("body", body);

    let category = Array.isArray(req.body.category) == true ? req.body.category : [req.body.category]
    //console.log("category", category);

    let serialNumber = Array.isArray(req.body.serialNumber) == true ? req.body.serialNumber : [req.body.serialNumber]
    //console.log("serialNumber", serialNumber);

    let service = Array.isArray(req.body.service) == true ? req.body.service : [req.body.service];
    //console.log("service", serialNumber);

    let quantity = Array.isArray(req.body.quantity) == true ? req.body.quantity : [req.body.quantity];
    // console.log("service", service);

    let finalPrice = Array.isArray(req.body.finalPrice) == true ? req.body.finalPrice : [req.body.finalPrice];
    // console.log("finalPrice", finalPrice);

    let jobsDone = [];
    for (var i = 0; i < serialNumber.length; i++) {
        let jobs = {
            serialNumber: serialNumber[i],
            category: category[i],
            service: service[i],
            quantity: quantity[i],
            finalPrice: finalPrice[i]
        }
        // console.log("jobs for loop", jobs);
        jobsDone.push(jobs);
    }
    //console.log("jobsDone2", jobsDone);
    
    let invoice = new Invoice(req.body);
    invoice.jobsDone = jobsDone
    //console.log("invoice", invoice);
    invoice.save().then(inv => {
        res.status(201).json({
            message: 'Invoice saved succesfully',
            inv
        })
    }).catch(err => console.log(err));
});

router.put('/:id', (req, res, next) => {
    let id = req.params.id;
    let Obj = req.body;

    Invoice.findByIdAndUpdate(id, {
            $set: Obj,
            $inc: {
                __v: 1
            }
        }, {
            new: true
        })
        .then((data) => {
            res.json({
                data: data
            })
        }).catch(err => next(err))
});

router.get('/', async (req, res, next) => {
    var filter = {};
    //console.log(req.query);
    if (req.query.name) {
        filter.name = req.query.name
    }
    if (req.query.phoneNumber) {
        filter.phoneNumber = req.query.phoneNumber
    }
    if (req.query.invoiceNumber) {
        filter.invoiceNumber = req.query.invoiceNumber
    }
    if (req.query.customerId) {
        filter.customerId = req.query.customerId;
    }
    if (req.query.Paymentstatus) {
        filter.Paymentstatus = req.query.Paymentstatus;
    }
    if (req.query.status) {
        filter.status = req.query.status;
    }
    try {
        let invoices;
        let findQuery;
        let foundDocuments;
        let query = req.query.name;
        //  console.log("quewry", query);
        //console.log("filter", filter);
        let count = await Invoice.find(filter).countDocuments();
        if (query) {
            //  console.log("inside if", query)
            findQuery = await Invoice.find({
                $text: {
                    $search: query
                }
            });
            //console.log("findQuery", findQuery);
            foundDocuments = await Invoice.find({
                $text: {
                    $search: query
                }
            }).countDocuments();
        } else {
            //console.log("filter", filter);
            invoices = await Invoice.find(filter).sort({
                _id: -1
            });
            //console.log("else invoices", invoices)
        }
        //console.log("findQuery", findQuery);
        res.status(200).json({
            invoices: invoices,
            foundDocuments: foundDocuments,
            findQuery: findQuery,
            total: count
        })
    } catch (error) {
        next(error)
    }
});

router.delete('/:id', (req, res, next) => {
    let id = req.params.id;
    Invoice.findByIdAndDelete(id)
        .then((inv) => {
            res.status(200).json({
                message: 'Invoice deleted',
                data: inv
            });
        }).catch(err => next(err))
});


router.get('/:id', (req, res, next) => {
    var id = req.params.id;
    //console.log("id", id);
    Invoice.findById(id)
        .then(id => {
            //console.log(id)
            res.status(200).json({
                data: id
            })
        })
});

module.exports = router;