const router = require('express').Router();
const Shops = require('../schema/shopsSchema');
const Brands = require('../schema/brandsSchema');

router.get('/', async (req, res) => {
    //var type = req.query.type
    var filter = {};
    //console.log(req.query);

    if (req.query.brandId) {
        filter.brandId = req.query.brandId
    };
    if (req.query.brandName) {
        filter.brandName = req.query.brandName
    }
    if (req.query.ownerName) {
        filter.ownerName = req.query.ownerName
    }
    if (req.query.phoneNumber) {
        filter.phoneNumber = req.query.phoneNumber
    }
    if (req.query._id) {
        filter._id = req.query._id
    }
    try {
        let pgsizes;
        let pgnos;
        let count = await Shops.find(filter).countDocuments();
        //let shop = await Shops.find(filter).sort(req.query.sort);
        pgsizes = parseInt(req.query.pgsizes) || 10;
        pgnos = parseInt(req.query.pgnos) || 1;
        list = await Shops.find(filter).sort({
            createdOn: 1,
            serialNo: 1
        }).skip((pgsizes * pgnos) - pgsizes).limit(pgsizes);
        list.pgsizes = pgsizes;
        list.pgnos = pgnos;
        // console.log("list service pagination", list);
        res.status(200).json({
            //shops: shop,
            total: count,
            list: list,
            pgnos: pgnos,
            pgsizes: pgsizes
        })
    } catch (error) {
        console.log(error)
        next(error)
    }
});

router.get('/:id', async (req, res, next) => {
    var id = req.params.id;
    Shops.findById(id)
        .then(id => {
            console.log(id)
            res.status(200).json({
                message: `Details of the selected shop Id ${id._id}`,
                data: id
            })
        })
});

router.post('/', async (req, res, next) => {
    let body = req.body;
    console.log("body data ==>", body);

    try {
        let brand = await Brands.find({
            brandId: body.brandId
        });
        // console.log("brands", brand);
        let shop = new Shops(req.body);
        shop.brandDetails = brand;
        // console.log("new shop created", shop.brandDetails[0]._id);
        let newShop = await shop.save();
        // console.log("newShop ==>", newShop);
        let count = await Shops.find({
            brandId: shop.brandId
        }).countDocuments();
        // console.log("count new", count);
        let updateBrands = await Brands.findByIdAndUpdate(shop.brandDetails[0]._id, {
            $set: {
                totalShops: count
            },
            $inc: {
                __v: 1
            }
        }, {
            new: true
        })
        // console.log("updateBrands", updateBrands);
        res.status(201).json({
            mesaage: 'Shop details saved successfully',
            newShop,
            //count
        })
    } catch (error) {
        next(error)
    }
});

router.delete('/:id', (req, res, next) => {
    let id = req.params.id;
    Shops.findByIdAndDelete(id).then((shop) => {
        res.status(200).json({
            message: `Shop deleted with Id ${id}`,
            data: shop
        });
    }).catch(err => next(err))
});

router.put('/:id', async (req, res, next) => {
    let id = req.params.id;
    let shopObj = req.body;

    Shops.findByIdAndUpdate(id, {
        $set: shopObj,
        $inc: {
            __v: 1
        }
    }, {
        new: true
    }).then((data) => {
        res.json({
            message: `Updated details of the selected Shop Id ${id}`,
            data: data
        })
    }).catch(err => next(err))
});

module.exports = router;