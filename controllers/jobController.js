const router = require('express').Router();
const Job = require('../schema/jobSchema');
const Service = require('../schema/serviceSchema');
const Category = require('../schema/categorySchema');


router.get('/', async (req, res, next) => {
    var filter = {};
    if (req.query.phoneNumber) {
        filter.phone = req.query.phoneNumber
    }
    if (req.query.name) {
        filter.name = req.query.name
    }
    try {
        let count = await Job.find(filter).countDocuments();
        let job = await Job.find(filter)
        res.status(200).json({
            data: job,
            total: count
        })
    } catch (error) {
        next(error)
    }
});


router.post('/', async (req, res) => {
    let ids = req.body;
    let catId = ids.categoryId;
   // console.log(ids);
   
   // console.log("catId", catId)
   // console.log("catId.length", catId.length)
    let subcatId = ids.subCategoryId;
   // console.log("subcatId", subcatId); 
   // console.log("subcatId.length", subcatId.length)
    let serId = ids.serviceId;
   // console.log("serId", serId);
   // console.log("serId.length", serId.length)
   
    let job = new Job(ids);
    for (i = 0; i < catId.length; i++) {
        Category.findById(catId[i])
            .then(category => {
                if (category && category != null) {
     //               console.log('category.name', category.name);
                    job.category.push(`${category.name}`)
       //             console.log( job.category);
                   
                }
            })
    }
    for (i = 0; i < subcatId.length; i++) {
        Service.findById(subcatId[i])
            .then(service => {
                if (service && service != null) {
         //           console.log('service.name', service.name);
                    job.subcategory.push(`${service.name}`)
           //         console.log(job.service);
                 
                }
            })
    }
    for (i = 0; i < serId.length; i++) {
        Subservice.findById(serId[i])
            .then(subService => {
                if (subService && subService != null) {
             //       console.log('subservice.name', subService.subService);
                    job.service.push(`${subService.subService}`)
               //   console.log(job.subService);
                }
            })
    }
    // var category = catgy.concat();
    //console.log("category1", category)
    // var service = subcatgy.concat();
    //console.log('service2', service);
    // var subService = subser.concat();
    //console.log('subService3',subService);
    // console.log(job);
    job.save().then(job => {
        res.status(201).json({
            message: 'Job saved successfully', job
        })
    }).catch(err => console.log(err));
});

module.exports = router;