const router = require('express').Router();
const UserProfile = require('../schema/userProfileSchema');
const md5 = require('md5');
const jwt = require('jsonwebtoken');


router.post('/', async (req, res, next) => {
    var username = req.body.username;
    var password = md5(req.body.password);
    // console.log("username", username, "password", password);

    const checkUser = await UserProfile.findOne({
        username: req.body.username,
        password: md5(req.body.password)
    })
    // console.log("checkUser", checkUser);

    if (!checkUser) return res.json({
        code: 400,
        error: 'Username or password not correct'
    })
    if (!checkUser.orgApiKey) return res.json({
        code: 400,
        message: 'Invalid user'
    })
    
    const token = jwt.sign({
        id: checkUser._id
    }, 'autopylo@key', {
        expiresIn: 24 * 60 * 60 * 60
    });

    res.json({
        "data": checkUser._id,
        "code": 200,
        "token": token
    });

    // console.log('token generated', token);

    // UserProfile.find({
    //     'username': username,
    //     'password': password
    // }, (err, user) => {
    //     if (err) {
    //         console.log("user info ", user)
    //         res.json({
    //             error: err
    //         });
    //     } else if (user.length < 1) {
    //         res.json({
    //             error: {
    //                 msg: "Invalid username or passsword"
    //             }
    //         });
    //     } else {
    //         res.json({
    //             code: 200,
    //             data: user,
    //             // token: token
    //         });
    //     }
    // });
});



module.exports = router;