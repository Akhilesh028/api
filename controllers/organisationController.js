const router = require('express').Router();
const Organisation = require('../schema/organisationSchema');
const uuidv4 = require('uuid/v4');

router.delete('/:id', (req, res, next) => {
    let id = req.params.id;
    Organisation.findByIdAndDelete(id).then((org) => {
        res.status(200).json({
            message: 'org deleted',
            data: org
        });
    }).catch(err => next(err))
});

router.post('/', (req, res) => {
    let body = req.body;
    // console.log("body", body);
    let org = new Organisation(req.body);
    org.apiKey = uuidv4();
    // console.log("org new ", org);
    org.save().then(organisatiton => {
        res.status(201).json({
            mesaage: 'Organisation saved successfully',
            organisatiton
        })
    }).catch(err => console.log(err));
});

router.get('/', async (req, res) => {
    var filter = {};

    if (req.query.orgName) {
        filter.orgName = req.query.orgName
    };
    if (req.query.orgId) {
        filter.orgId = req.query.orgId
    }
    try {
        let count = await Organisation.find(filter).countDocuments();
        let org = await Organisation.find(filter).sort(req.query.sort);
        res.status(200).json({
            org: org,
            total: count
        })
    } catch (error) {
        next(error)
    }
});

router.put('/:id', async (req, res, next) => {
    let id = req.params.id;
    let orgObj = req.body;

    Organisation.findByIdAndUpdate(id, {
        $set: orgObj,
        $inc: {
            __v: 1
        }
    }, {
        new: true
    }).then((data) => {
        res.json({
            data: data
        })
    }).catch(err => next(err))
});

module.exports = router;