const router = require('express').Router();
const UserProfile = require('../schema/userProfileSchema');
const md5 = require('md5');
// const Brands = require('../schema/brandsSchema');

router.get('/', async (req, res) => {
    let filter = {};
    if (req.query.orgApiKey) {
        filter.orgApiKey = req.query.orgApiKey
    };
    if (req.query.brandId) {
        filter.brandId = req.query.brandId
    };
    if (req.query.brandApiKey) {
        filter.brandApiKey = req.query.brandApiKey
    };
    if (req.query.phoneNumber) {
        filter.phoneNumber = req.query.phoneNumber
    };
    if (req.query.username) {
        filter.username = req.query.username
    };
    if (req.query.emailId) {
        filter.emailId = req.query.emailId
    };

    try {
        let userProfile = await UserProfile.find(filter);
        console.log(filter)
        let count = await UserProfile.find(filter).countDocuments();
        console.log("userProfile", userProfile);
        console.log("count", count);

        res.status(200).json({
            userProfile: userProfile,
            count: count
        })
    } catch (error) {
        console.log(error)
        next(error)
    }
});

router.get('/:id', async (req, res, next) => {
    let id = req.params.id;
    UserProfile.findById(id)
        .then(id => {
            // console.log(id)
            res.status(200).json({
                message: `Details of the selected userProfile Id ${id._id}`,
                id
            })
        })
});

router.post('/', async (req, res) => {
    let body = req.body;
    console.log("body ", body);

    let userProfile = new UserProfile(body);
    userProfile.password = md5(body.password);
    userProfile.save().then(userProfile => {
        res.status(201).json({
            message: 'UserProfile saved successfully',
            userProfile
        })
    }).catch(err => console.log(err));
});

router.delete('/:id', (req, res, next) => {
    let id = req.params.id;
    UserProfile.findByIdAndDelete(id).then((userProfile) => {
        res.status(200).json({
            message: `UserProfile deleted with Id ${id}`,
            data: userProfile
        });
    }).catch(err => next(err))
});

router.put('/:id', async (req, res, next) => {
    let id = req.params.id;
    let Obj = req.body;

    UserProfile.findByIdAndUpdate(id, {
        $set: Obj,
        $inc: {
            __v: 1
        }
    }, {
        new: true
    }).then((data) => {
        res.json({
            message: `Updated details of the selected UserProfile Id ${id}`,
            data
        })
    }).catch(err => next(err))
});

module.exports = router;