const router = require('express').Router();
const EmpPreviousJobDetails = require('../schema/empPreviousJobDetailsSchema');
const Employee = require('../schema/employeeSchema');


router.post('/', async (req, res, next) => {
    let empJob = req.body;
    let filter = {};
    if (!empJob.employeeID) {
        res.status(400).json({
            message: "employeeID not found"
        })
    }
    if (req.body.employeeID) {
        filter.employeeID = req.body.employeeID
    };
    try {
        Employee.find(filter)
            .then(employee => {
             //   console.log("find employee", employee)
                if (employee && employee != null) {
                    empJob.employee = employee
                 //   console.log("empJob.employee",employee[0]._id)
                    let newEmployee = new EmpPreviousJobDetails(empJob);
                 //   console.log("newEmployee",newEmployee);
                    newEmployee.empId = employee[0]._id;
                 //   console.log("newEmployee.empId",newEmployee.empId)
                    newEmployee.save()
                        .then(empJob => {
                            res.status(201).json({
                                message: 'Employee Previous Job Details created successfully',
                                empJob
                            })
                        }).catch(err => next(err))
                } else {
                    res.status(404).json({
                        message: 'Employee not found'
                    })
                }
            }).catch(err => next(err))
    } catch (error) {
        next(error)
    }
});

router.get('/:id', (req, res, next) => {
    var id = req.params.id;
    EmpPreviousJobDetails.findById(id)
        .then(id => {
          //  console.log(id)
            res.status(200).json({
                data: id
            })
        })
});


router.get('/', async (req, res, next) => {
    var fil = {};
  //  console.log(req.query);

    if (req.query.companyName) {
        fil.companyName = req.query.companyName
    };
    if (req.query.phoneNumbers) {
        fil.phoneNumbers = req.query.phoneNumbers
    };
    try {
       // console.log('hiiii')
        let empPre = await EmpPreviousJobDetails.find(fil).sort(req.query.sort);
        res.status(200).json({
            data: empPre
        })
    } catch (error) {
        next(error)
    }
});

router.delete('/:id', (req, res, next) => {
    let id = req.params.id;
    EmpPreviousJobDetails.findByIdAndDelete(id).then((empPre) => {
        res.status(200).json({
            message: 'Emp Previous Job Details deleted',
            data: empPre
        });
    }).catch(err => next(err))
});

router.put('/:id', (req, res, next) => {
    let id = req.params.id;
    let empObj = req.body;
    EmpPreviousJobDetails.findByIdAndUpdate(id, {
            $set: empObj,
            $inc: {
                __v: 1
            }
        }, {
            new: true
        })
        .then((data) => {
            res.json({
                data: data
            })
        }).catch(err => next(err))
})

module.exports = router;