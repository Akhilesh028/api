const router = require('express').Router();
const Brands = require('../schema/brandsSchema');
const Organisation = require('../schema/organisationSchema');
const uuidv4 = require('uuid/v4');
const shortid = require('shortid');


router.get('/', async (req, res) => {
    //var type = req.query.type
    var filter = {};
    // console.log(req.query);

    if (req.query.brandId) {
        filter.brandId = req.query.brandId
    };
    if (req.query.brandName) {
        filter.brandName = req.query.brandName
    }
    if (req.query.ownerName) {
        filter.ownerName = req.query.ownerName
    }
    if (req.query.phoneNumber) {
        filter.phoneNumber = req.query.phoneNumber
    }
    try {
        let psizes;
        let pnos;
        // let serialNo;
        // serialNo = await ((Brands.find().sort({
        //     createdOn: -1,
        //     serialNo: -1
        // }).limit(1)[0].serialNo) === undefined ? 1 : ((Brands.find().sort({
        //     createdOn: -1,
        //     serialNo: -1
        // })[0].serialNo) + 1));    
        // console.log("serialNo ======>>>>>>", serialNo);
        let count = await Brands.find(filter).countDocuments();
        psizes = parseInt(req.query.psizes) || 10;
        pnos = parseInt(req.query.pnos) || 1;
        let brands = await Brands.find(filter).sort(req.query.sort).skip((psizes * pnos) - psizes).limit(psizes);
        brands.psizes = psizes;
        brands.pnos = pnos;
        res.status(200).json({
            brands: brands,
            total: count,
            pnos: pnos,
            psizes: psizes,
            //serialNo: serialNo
        })
    } catch (error) {
        console.log(error)
        next(error)
    }
});

router.get('/:id', async (req, res, next) => {

    var id = req.params.id;
    Brands.findById(id)
        .then(id => {
            // console.log(id)
            res.status(200).json({
                message: `Details of the selected brand Id ${id._id}`,
                data: id
            })
        })
});

router.post('/', async (req, res) => {
    let body = req.body;
    let org = await Organisation.find();
    let brand = new Brands(req.body);
    brand.brandId = shortid.generate();
    brand.apiKey = uuidv4();
    brand.orgKey = org[0].orgKey;
    brand.orgId = org[0]._id;
    brand.save().then(brand => {
        res.status(201).json({
            mesaage: 'brand saved successfully',
            brand
        })
    }).catch(err => console.log(err));
});

router.delete('/:id', (req, res, next) => {
    let id = req.params.id;
    Brands.findByIdAndDelete(id).then((brand) => {
        res.status(200).json({
            message: `brand deleted with Id ${id}`,
            data: brand
        });
    }).catch(err => next(err))
});

router.put('/:id', async (req, res, next) => {
    let id = req.params.id;
    let brandObj = req.body;

    Brands.findByIdAndUpdate(id, {
        $set: brandObj,
        $inc: {
            __v: 1
        }
    }, {
        new: true
    }).then((data) => {
        res.json({
            message: `Updated details of the selected brand Id ${id}`,
            data: data
        })
    }).catch(err => next(err))
});




module.exports = router;