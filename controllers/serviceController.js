const router = require('express').Router();
const Service = require('../schema/serviceSchema');
const Category = require('../schema/categorySchema');
const Membership = require('../schema/membershipSchema');

router.get('/', async (req, res, next) => {
    var filter = {};
    console.log(req.query);
    if (req.query.categoryId) {
        filter.categoryId = req.query.categoryId
    }
    if (req.query.name) {
        filter.name = req.query.name
    }
    if (req.query.type) {
        filter.type = req.query.type
    }
    if (req.query.rate) {
        filter.rate = req.query.rate
    }
    if (req.query.membershipId) {
        filter.membershipId = req.query.membershipId;
    }
    try {
        let serviceId;
        let services;
        let findQuery;
        let foundDocuments;
        let list;
        let psize;
        let pno;
        let query = req.query.name;
        let count = await Service.find(filter).countDocuments();
        if (query) {
            findQuery = await Service.find({
                $text: {
                    $search: query
                }
            });
            foundDocuments = await Service.find({
                $text: {
                    $search: query
                }
            }).countDocuments();
        } else {
            services = await Service.find(filter).sort({
                createdOn: 1,
                serialNo: 1
            });
            serviceId = (await Service.find().sort({
                createdOn: -1,
                serialNo: -1
            }).limit(1))[0].serialNo;
            // console.log("serviceId", serviceId);
            psize = parseInt(req.query.psize) || 10;
            pno = parseInt(req.query.pno) || 1;
            list = await Service.find(filter).sort({
                createdOn: 1,
                serialNo: 1
            }).skip((psize * pno) - psize).limit(psize);
            list.psize = psize;
            list.pno = pno;
            // console.log("list service pagination", list);
        }
        //console.log("findQuery", findQuery);
        res.status(200).json({
            services: services,
            foundDocuments: foundDocuments,
            findQuery: findQuery,
            total: count,
            pno: pno,
            psize: psize,
            list: list,
            serId: serviceId
            //current: pno,
            //pages: Math.ceil(count / psize)
        })
    } catch (error) {
        next(error)
    }
});

router.get('/:id', async (req, res, next) => {
    var id = req.params.id;
    Service.findById(id)
        .then(id => {
            console.log(id)
            res.status(200).json({
                data: id
            })
        })
});

router.get('/getByGroup', (req, res, next) => {
    Service.aggregate([{
        $group: {
            _id: "$category",
            "services": {
                $push: "$$ROOT"
            }
        }
    }]).then((data) => {
        res.json({
            data
        })
    }).catch(err => next(err))
});

router.put('/:id', async (req, res, next) => {
    let id = req.params.id;
    let ServiceObj = req.body;
    // console.log(req.body)

    Service.findByIdAndUpdate(id, {
        $set: ServiceObj,
        $inc: {
            __v: 1
        }
    }, {
        new: true
    }).then((data) => {
        // console.log('updated service', data)
        res.json({
            data: data
        })
    }).catch(err => next(err))
});

router.post('/', async (req, res, next) => {
    let service = req.body;
    let fil = {};
    // console.log("service in request,body", service);
    if (!service.categoryId) {
        res.status(400).json({
            message: "Category Id not found"
        })
    }
    if (!service.membershipId) {
        res.status(400).json({
            message: "Membership Id not found"
        })
    }
    if (req.body.membershipId) {
        fil.membershipId = req.body.membershipId
        // console.log("fil.membershipId", fil.membershipId)
    };
    let membership = await Membership.findById(service.membershipId);
    // console.log("fil", fil);
    // console.log("membership", membership)
    service.membership = membership;
    // console.log("body.membership", service.membership);
    Category.findById(service.categoryId).then(category => {
        if (category && category != null) {
            service.category = category;
            let newService = new Service(service);
            newService.save().then(service => {
                res.status(201).json({
                    message: 'Service created successfully',
                    service
                })
            }).catch(err => next(err))
        } else {
            res.status(404).json({
                message: 'category not found'
            })
        }
    }).catch(err => next(err))
});

router.delete('/:id', (req, res, next) => {
    let id = req.params.id;
    Service.findByIdAndRemove(id).then((Service) => {
        res.status(200).json({
            message: 'Service deleted',
            data: Service
        });
    }).catch(err => next(err))
});


module.exports = router;