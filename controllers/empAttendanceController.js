const router = require('express').Router();
const EmpAttendance = require('../schema/empAttendanceSchema');
const Employee = require('../schema/employeeSchema');

router.post('/', async (req, res, next) => {
    let empattendance = req.body;
    if (!empattendance.employeeId) {
        res.status(400).json({
            message: "EmployeeId not found"
        })
    }
    Employee.findById(empattendance.employeeId)
        .then(employee => {
            if (employee && employee != null) {
                empattendance.employee = employee
                let newEmployee = new EmpAttendance(empattendance);
                newEmployee.save()
                    .then(empattendance => {
                        res.status(201).json({
                            message: 'Employee Attendance created successfully',
                            empattendance
                        })
                    }).catch(err => next(err))
            } else {
                res.status(404).json({
                    message: 'Employee not found'
                })
            }
        }).catch(err => next(err))
})

router.get('/', async (req, res, next) => {
    var fil = {};
   // console.log(req.query);
    if (req.query.shiftStartTime) {
        fil.shiftStartTime = req.query.shiftStartTime
    };
    if (req.query.shiftEndTime) {
        fil.shiftEndTime = req.query.shiftEndTime
    };
    try {
      //  console.log('hiiii')
        let emp = await EmpAttendance.find(fil).sort(req.query.sort);
        res.status(200).json({
            data: emp
        })
    } catch (error) {
        next(error)
    }
});

router.delete('/:id', (req, res, next) => {
    let id = req.params.id;
    EmpAttendance.findByIdAndDelete(id).then((empAtt) => {
        res.status(200).json({
            message: 'Emp Attendance deleted',
            data: empAtt
        });
    }).catch(err => next(err))
});


router.put('/:id', (req, res, next) => {
    let id = req.params.id;
    let empObj = req.body;
    EmpAttendance.findByIdAndUpdate(id, {
                $set: empObj,
                $inc: {
                    __v: 1
                }
            },
            {
                new: true
            })
        .then((data) => {
            res.json({
                data: data
            })
        }).catch(err => next(err))
})

module.exports = router;