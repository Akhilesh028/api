const router = require('express').Router();
const Tax = require('../schema/taxSchema');

router.post('/', (req, res) => {
    var tax = new Tax(req.body);
    //console.log("tax", tax);
    tax.save().then(taxes => {
        res.status(201).json({
            message: 'Tax saved succesfully',
            taxes
        })
    }).catch(err => console.log(err));
});


router.get('/', async (req, res) => {
    var filter = {};
   // console.log(req.query);

    if (req.query.name) {
        filter.name = req.query.name
    };
    if (req.query.type) {
        filter.type = req.query.type
    }
    try {
        let count = await Tax.find(filter).countDocuments();
        let taxes = await Tax.find(filter).sort(req.query.sort);
        res.status(200).json({
            data: taxes
        })
    } catch (error) {
        next(error)
    }
});

router.get('/:id', (req, res, next) => {
    var id = req.params.id;
    //console.log("id", id);
    Tax.findById(id)
        .then(id => {
      //      console.log(id)
            res.status(200).json({
                data: id
            })
        })
});

router.put('/:id', async (req, res, next) => {
    let id = req.params.id;
    let taxObj = req.body;
    //console.log("taxObj", taxObj);

    Tax.findByIdAndUpdate(id, {
        $set: taxObj,
        $inc: {
            __v: 1
        }
    }, {
        new: true
    }).then((data) => {
      //  console.log('updated tax', data)
        res.json({
            data: data
        })
    }).catch(err => next(err))
});

router.delete('/:id', (req, res, next) => {
    let id = req.params.id;
    Tax.findByIdAndRemove(id).then((tax) => {
        res.status(200).json({
            message: 'tax deleted',
            data: tax
        });
    }).catch(err => next(err))
});

module.exports = router;