const router = require('express').Router();
const Membership = require('../schema/membershipSchema');
const Customer = require("../schema/customerSchema");



router.post('/', (req, res) => {
    var memberhsip = new Membership(req.body);
    memberhsip.save().then(member => {
        res.status(201).json({
            message: 'membership saved succesfully',
            member
        })
    }).catch(err => console.log(err));
});


// router.post('/', async (req, res, next) => {
//     let membershipDetails = req.body;
//     console.log("membershipDetails", membershipDetails);
//     let filter = {};
//     if (!membershipDetails.customerId) {
//         res.status(400).json({
//             message: "customerId not found"
//         })
//     }
//     if (req.body.customerId) {
//         filter.customerId = req.body.customerId
//     };
//     try {
//         Customer.find(filter)
//             .then(customer => {
//                 console.log("find customer", customer)
//                 if (customer && customer != null) {
//                     membershipDetails.customer = customer
//                     console.log("membershipDetails.customer", customer[0].customerId)
//                     let newCustomer = new Membership(membershipDetails);
//                     console.log("newCustomer", newCustomer);
//                     newCustomer.custId = customer[0]._id;
//                     console.log("newCustomer.custId",newCustomer.custId)
//                     newCustomer.save()
//                         .then(membershipDetails => {
//                             res.status(201).json({
//                                 message: 'Employee Previous Job Details created successfully',
//                                 membershipDetails
//                             })
//                         }).catch(err => next(err))
//                 } else {
//                     res.status(404).json({
//                         message: 'Employee not found'
//                     })
//                 }
//             }).catch(err => next(err))
//     } catch (error) {
//         next(error)
//     }
// });

router.get('/', async (req, res, next) => {
    var fil = {};
   // console.log(req.query);
    if (req.query.name) {
        fil.name = req.query.name
    };
    if (req.query.membershipId) {
        fil.membershipId = req.query.membershipId
    };
    if (req.query.type) {
        fil.type = req.query.type
    };
    try {
        let membership = await Membership.find(fil).sort(req.query.sort);
     //   console.log("membership", membership);
        res.status(200).json({
            data: membership
        })
    } catch (error) {
        next(error)
    }
});

router.get('/:id', (req, res, next) => {
    var id = req.params.id;

    Membership.findById(id)
        .then(id => {
       //     console.log(id)
            res.status(200).json({
                data: id
            })
        })
})

router.delete('/:id', (req, res, next) => {
    let id = req.params.id;
    Membership.findByIdAndDelete(id).then((membership) => {
        res.status(200).json({
            message: 'Membership plan deleted',
            data: membership
        });
    }).catch(err => next(err))
});

router.put('/:id', (req, res, next) => {
    let id = req.params.id;
    // console.log("id", id);
    let membershipObj = req.body;
    // console.log("membership", membershipObj);
    Membership.findByIdAndUpdate(id, {
                $set: membershipObj,
                $inc: {
                    __v: 1
                }
            },

            {
                new: true
            })
        .then((data) => {
            res.json({
                message: "Membership plan successfully updated",
                data: data
            })
        }).catch(err => next(err))
});

module.exports = router;