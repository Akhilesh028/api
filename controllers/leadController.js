const router = require('express').Router();
const Lead = require('../schema/leadSchema');
const Customer = require('../schema/customerSchema');
const Invoice = require('../schema/finalInvoiceSchema');


router.get('/', async (req, res, next) => {
    var filter = {};
    if (req.query.phoneNumber) {
        filter.phone = req.query.phoneNumber
    };
    if (req.query.name) {
        filter.name = req.query.name
    }
    if (req.query.status) {
        filter.status = req.query.status
    }
    if (req.query.customerId) {
        filter["user.customerId"] = req.query.customerId;
    }
    try {
        let count = await Lead.find(filter).countDocuments();
        let lead = await Lead.find(filter).sort({
            token: -1
        }).sort({
            createdOn: -1
        }).limit(15);
        let latest = await Lead.find(filter).sort({
            _id: -1
        }).limit(15);
       // console.log("latest in the get request of lead sch", latest);
       // console.log("lead in the get request of lead sch", lead);
        res.status(200).json({
            data: lead,
            latest: latest,
            total: count
        })
    } catch (error) {
        next(error)
    }
});

router.get('/:id', async (req, res, next) => {
    let previousInvoice = await Invoice.find().sort({
        _id: -1
    }).limit(1);
    let preInv = (previousInvoice[0] && previousInvoice[0].invoiceNumber ? previousInvoice[0].invoiceNumber : "K&K-000");
  //  console.log("previous invoice", preInv);
    var lead = req.params.id;
    Lead.findById(lead)
        .then(lead => {
        //    console.log(lead)
            res.status(200).json({
                data: lead,
                prevInv: preInv
            })
        })
})


router.post('/', async (req, res, next) => {
    let filter = {};
   // console.log("filter in post", filter);
    let lead = req.body;
    //console.log("lead", lead)
    let newLead = new Lead(lead);
    var today = new Date();
    let todayLeads = await Lead.find({
        createdOn: {
            $gte: new Date(today.setHours(00, 00, 00)),
            $lt: new Date(today.setDate(today.getDate() + 1))
        }
    }).countDocuments();
    //console.log('todayLeads', todayLeads) // 0
    newLead.token = todayLeads + 1;
   // console.log('newLead.token  ', newLead.token)
    if (lead.userId) {
     //   console.log('first if', lead.userId);
        Customer.findById(lead.userId)
            .then(user => {
       //         console.log(user)
                if (user && user != null) {
         //           console.log('user')
                    newLead.user = user;
                    newLead.save().then(leadCreated => {
           //             console.log('lead created', leadCreated)
                        res.status(201).json({
                            message: 'Lead created successfully',
                            leadCreated
                        })
                    }).catch(err => {
                        console.log(err)
                        next(err)
                    })
                } else {
                    res.status(404).json({
                        message: `User found with ${userId}`
                    })
                }
            })
    } else {
        //console.log('else part')
        if (lead.userinformat) {
          //  console.log('if part')
            let last = await Customer.find(filter).sort({
                _id: -1
            }).limit(1);
            //console.log("last", last);
            //console.log(last[0].customerId)
            var lastID = last && last.length > 0 ? last[0].customerId : 0 // last customerId
            //console.log("lasID", lastID);
            var newUser = new Customer(JSON.parse(lead.userinformat));
           // console.log("newUser", newUser)
            newUser.customerId = parseInt(lastID) + 1;
            // newUser.customerId = lastID) + 1 : 0;
           // console.log("after adding newUser", newUser);
            newUser.save().then(usr => {
                newLead.user = usr;
             //   console.log(usr)
                newLead.save().then(ld => {
                    res.status(201).json({
                        message: 'Lead created successfully',
                        ld
                    })
                }).catch(err => next(err))
            }).catch(err => next(err))
        } else {
            return res.status(404).json({
                message: 'Customer not found'
            })
        }
    }
})


router.put('/:id', async (req, res, next) => {
    let id = req.params.id;
    let LeadObj = req.body;
  //  console.log("leadObj", LeadObj);

    Lead.findByIdAndUpdate(id, {
        $set: LeadObj,
        $inc: {
            __v: 1
        }
    }, {
        new: true
    }).then((data) => {
        res.json({
            data: data
        })
    }).catch(err => next(err))
});


router.delete('/:id', (req, res, next) => {
    let id = req.params.id;
    Lead.findByIdAndDelete(id)
        .then((Lead) => {
            res.status(200).json({
                message: 'Lead deleted',
                data: Lead
            });
        }).catch(err => next(err))
});


module.exports = router;